package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import dao.JPA_Nazione_DAO;
import orm.Campionato;
import orm.Nazione;
import orm.Squadra;
import service.NazioneService;

class testNazione {
	
	static EntityManagerFactory emf =	Persistence.createEntityManagerFactory("JPA_Project");
	static EntityManager em = emf.createEntityManager();
	public static JPA_Nazione_DAO nazionedao = new JPA_Nazione_DAO();
	NazioneService ns = new NazioneService(nazionedao,em);
	
	/** Metodo per ripulire le tabelle, alla fine di ogni test, dai record aggiunti al DB */
	@AfterEach
	public void afterMethods() {
		ns.dropNation();
	}
	
	/** Metodo per chiudere l'EntityManager alla fine di tutti test */
	@AfterAll
	public static void closeConnection() {
		em.close();
	}
	
	@Test
	void createNation() {
		ns.createNation("Italia", "Europa");
		ns.createNation("Germania", "Europa");
		ns.createNation("Francia", "Europa");
		long n = ns.countNation();
		assertEquals(3,n);
		boolean b = ns.createNation("Zimbawe",null);
		assertEquals(b,false);
		ns.dropNation();
		n = ns.countNation();
		assertEquals(0,n);	
	}
	
	@Test
	void getNation() {
		ns.createNation("Italia", "Europa");
		Nazione i = ns.findNationByName("Italia");
		int id = i.getIdNazione();
		Nazione i1 = ns.getNation(id);
		assert(i.equals(i1));
		int id1 = -13;
		Nazione n1 = ns.getNation(id1);
		assertEquals(n1,null);
		Nazione n2 = ns.findNationByName(null);
		assertEquals(n2,null);
		ns.dropNation();		
	}
	
	@Test
	void getAllNations() {
		ns.createNation("Italia", "Europa");
		ns.createNation("Germania", "Europa");
		ns.createNation("Francia", "Europa");
		ns.createNation("Russia", "Asia");
		ns.createNation("Cina", "Asia");
		List<Nazione> nazioni = ns.findAllNations();
		int n = nazioni.size();
		assertEquals(5,n);
		ns.dropNation();		
	}
	
	@Test
	void deleteNation() {
		ns.createNation("Italia", "Europa");
		Nazione i = ns.findNationByName("Italia");
		ns.deleteNation(i.getIdNazione());
		Throwable exception = assertThrows(javax.persistence.NoResultException.class, () 
				-> {  ns.findNationByName("Italia");;	});
		int id = -13;
		boolean b = ns.deleteNation(id);
		assertEquals(b,false);
		ns.dropNation();		
	}
	
	@Test
	void updateNation() {
		ns.createNation("Italia", "Europa");
		Nazione i = ns.findNationByName("Italia");
		int id = (int)i.getIdNazione();
		ns.updateNation(id, "Italy");
		Nazione i1 = ns.findNationByName("Italy");
		assertEquals(i.getIdNazione(),i1.getIdNazione());
		int id1 = -13;
		boolean b = ns.updateNation(id1,"Italy");
		assertEquals(b,false);
		boolean b1 = ns.updateNation(id,null);
		assertEquals(b1,false);
		ns.dropNation();
	}
	
	@Test
	void setter() {
		Nazione n = new Nazione();
		n.setContinente("Europa");
		n.setNomeNazione("Italia");	
	}
}
