package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import dao.JPA_Campionato_DAO;
import dao.JPA_Nazione_DAO;
import dao.JPA_Squadra_DAO;
import orm.Nazione;
import orm.Squadra;
import orm.Calciatore;
import orm.Campionato;
import service.CampionatoService;
import service.NazioneService;
import service.SquadraService;

class testSquadra {
	
	static EntityManagerFactory emf =	Persistence.createEntityManagerFactory("JPA_Project");
	static EntityManager em = emf.createEntityManager();
	public static JPA_Campionato_DAO campionatodao = new JPA_Campionato_DAO();
	CampionatoService cs = new CampionatoService(campionatodao,em);
	public static JPA_Nazione_DAO nazionedao = new JPA_Nazione_DAO();
	NazioneService ns = new NazioneService(nazionedao,em);
	public static JPA_Squadra_DAO squadradao = new JPA_Squadra_DAO();
	SquadraService ss = new SquadraService(squadradao,em);
	
	/** Metodo per ripulire le tabelle, alla fine di ogni test, dai record aggiunti al DB */
	@AfterEach
	public void afterMethods() {
		ss.dropTeam();
		cs.dropChampionship();
		ns.dropNation();
	}
	
	/** Metodo per chiudere l'EntityManager alla fine di tutti test */
	@AfterAll
	public static void closeConnection() {
		em.close();
	}

	@Test
	void createTeam() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		ss.createTeam("Milan", new GregorianCalendar(1899,4,18), c);
		ss.createTeam("Juventus", new GregorianCalendar(1940,5,2), c);
		ss.createTeam("Inter", new GregorianCalendar(1948,3,12), c);
		ss.createTeam("Fiorentina", new GregorianCalendar(1930,7,1), c);
		long i = ss.countTeam();
		assertEquals(4,i);
		boolean b = ss.createTeam("Genoa", new GregorianCalendar(1930,7,1), null);
		assertEquals(b,false);
		boolean b1 = ss.createTeam("Sassuolo", null, c);
		assertEquals(b1,false);
		ss.dropTeam();
		i = ss.countTeam();
		assertEquals(0,i);	
	}
	
	@Test
	void getTeam() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		ss.createTeam("Milan", new GregorianCalendar(1899,4,18), c);
		Squadra s = ss.findTeamByName("Milan");
		int id = s.getIdSquadra();
		Squadra s1 = ss.getTeam(id);
		assert(s.equals(s1));
		int id1 = -13;
		Squadra s2 = ss.getTeam(id1);
		assertEquals(s2,null);
		Squadra s3 = ss.findTeamByName(null);
		assertEquals(s2,null);	
	}
	
	@Test
	void getAllTeams() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		ss.createTeam("Milan", new GregorianCalendar(1899,4,18), c);
		ss.createTeam("Juventus", new GregorianCalendar(1940,5,2), c);
		ss.createTeam("Inter", new GregorianCalendar(1948,3,12), c);
		ss.createTeam("Fiorentina", new GregorianCalendar(1930,7,1), c);
		List<Squadra> squadre = ss.findAllTeam();
		int i = squadre.size();
		assertEquals(4,i);	
	}
	
	@Test
	void deleteTeam() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		ss.createTeam("Milan", new GregorianCalendar(1899,4,18), c);
		Squadra s = ss.findTeamByName("Milan"); 
		ss.deleteTeam(s.getIdSquadra());
		Squadra x = ss.findTeamByName("Milan");
		assertEquals(x,null);
		int id1 = -13;
		boolean b = ss.deleteTeam(id1);
		assertEquals(b,false);		
	}
		
	@Test
	void updateTeam() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		ss.createTeam("Milan", new GregorianCalendar(1899,4,18), c);
		Squadra s = ss.findTeamByName("Milan");
		int id = s.getIdSquadra();
		ss.updateTeamName(id, "AC Milan");
		Squadra s1 = ss.findTeamByName("AC Milan");
		assertEquals(s.getIdSquadra(),s1.getIdSquadra());
		int id1 = -13;
		boolean b = ss.updateTeamName(id1, "AC Milan");
		assertEquals(b,false);
		boolean b1 = ss.updateTeamName(id,null);
		assertEquals(b1,false);
	}
	
	@Test
	void setter() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		Squadra s = new Squadra();
		s.setCampionato(c);
		s.setDataFondazione(new GregorianCalendar(1899,4,18));
		s.setNomeSquadra("Milan");
	}
}
