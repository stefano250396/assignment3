package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import dao.JPA_Campionato_DAO;
import dao.JPA_Nazione_DAO;
import orm.Calciatore;
import orm.Campionato;
import orm.Nazione;
import orm.Squadra;
import service.CampionatoService;
import service.NazioneService;

class testCampionato {

	static EntityManagerFactory emf =	Persistence.createEntityManagerFactory("JPA_Project");
	static EntityManager em = emf.createEntityManager();
	public static JPA_Campionato_DAO campionatodao = new JPA_Campionato_DAO();
	CampionatoService cs = new CampionatoService(campionatodao,em);
	public static JPA_Nazione_DAO nazionedao = new JPA_Nazione_DAO();
	NazioneService ns = new NazioneService(nazionedao,em);
	
	/** Metodo per ripulire le tabelle, alla fine di ogni test, dai record aggiunti al DB */
	@AfterEach
	public void afterMethods() {
		cs.dropChampionship();
		ns.dropNation();
	}
	
	/** Metodo per chiudere l'EntityManager alla fine di tutti test */
	@AfterAll
	public static void closeConnection() {
		em.close();
	}
		
	@Test
	void createChampionship() {
		Nazione n = new Nazione("Germania","Europa");
		Nazione n1 = new Nazione("Italia","Europa");
		cs.createChampionship("Bundesliga", n);
		cs.createChampionship("SeriaA", n1);
		long i = cs.countChampionship();
		assertEquals(2,i);
		boolean b = cs.createChampionship("LIgue1", null);
		assertEquals(b,false);
		cs.dropChampionship();
		ns.dropNation();
		i = cs.countChampionship();
		assertEquals(0,i);	
	}
	
	@Test
	void getChampionship() {
		Nazione n1 = new Nazione("Italia","Europa");
		cs.createChampionship("SerieA", n1);
		Campionato c = cs.findChampionshipByName("SerieA");
		int id = c.getIdCampionato();
		Campionato c1 = cs.getChampionship(id);
		assert(c.equals(c1));
		int id1 = -13;
		Campionato c2 = cs.getChampionship(id1);
		assertEquals(c2,null);
		Campionato n2 = cs.findChampionshipByName(null);
		assertEquals(n2,null);	
	}
	
	@Test
	void getAllChampionships() {
		Nazione n = new Nazione("Germania","Europa");
		Nazione n1 = new Nazione("Italia","Europa");
		Nazione n2 = new Nazione("Francia","Europa");
		cs.createChampionship("Bundesliga", n);
		cs.createChampionship("SerieA", n1);
		cs.createChampionship("Ligue1", n1);
		List<Campionato> campionati = cs.findAllChampionship();
		int i = campionati.size();
		assertEquals(3,i);	
	}
	
	@Test
	void deleteChampionship() {
		Nazione n = new Nazione("Italia","Europa");
		cs.createChampionship("SerieA", n);
		Campionato c = cs.findChampionshipByName("SerieA");
		cs.deleteChampionship(c.getIdCampionato());
		Throwable exception = assertThrows(javax.persistence.NoResultException.class, () 
				-> {  cs.findChampionshipByName("SerieA");	});
		int id = -13;
		boolean b = cs.deleteChampionship(id);
		assertEquals(b,false);		
	}
	
	@Test
	void updateChampionship() {
		Nazione n = new Nazione("Italia","Europa");
		cs.createChampionship("SerieA", n);
		Campionato c = cs.findChampionshipByName("SerieA");
		int id = c.getIdCampionato();
		cs.updateChampionshipName(id, "SerieATim");
		Campionato c1 = cs.findChampionshipByName("SerieATim");
		assertEquals(c.getIdCampionato(),c1.getIdCampionato());
		int id1 = -13;
		boolean b = cs.updateChampionshipName(id1,"SerieATim");
		assertEquals(b,false);
		boolean b1 = cs.updateChampionshipName(id,null);
		assertEquals(b1,false);
	}	
	
	@Test
	void setter() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato();
		c.setNazione(n);
		c.setNomeCampionato("SerieA");
	}
}
