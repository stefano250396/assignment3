package test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import dao.JPA_Calciatore_DAO;
import dao.JPA_Campionato_DAO;
import dao.JPA_Nazione_DAO;
import dao.JPA_Squadra_DAO;
import orm.Calciatore;
import orm.Campionato;
import orm.Nazione;
import orm.Squadra;
import service.CalciatoreService;
import service.CampionatoService;
import service.NazioneService;
import service.SquadraService;

class testCalciatore {
	
	static EntityManagerFactory emf =	Persistence.createEntityManagerFactory("JPA_Project");
	static EntityManager em = emf.createEntityManager();
	public static JPA_Nazione_DAO nazionedao = new JPA_Nazione_DAO();
	NazioneService ns = new NazioneService(nazionedao,em);
	public static JPA_Campionato_DAO campionatodao = new JPA_Campionato_DAO();
	CampionatoService cs = new CampionatoService(campionatodao,em);
	public static JPA_Squadra_DAO squadradao = new JPA_Squadra_DAO();
	SquadraService ss = new SquadraService(squadradao,em);
	public static JPA_Calciatore_DAO calciatoredao = new JPA_Calciatore_DAO();
	CalciatoreService cls = new CalciatoreService(calciatoredao,em);
	
	/** Metodo per ripulire le tabelle, alla fine di ogni test, dai record aggiunti al DB */
	@AfterEach
	public void afterMethods() {	
		cls.dropSoccer();
		ss.dropTeam();
		cs.dropChampionship();
		ns.dropNation();
	}
	
	/** Metodo per chiudere l'EntityManager alla fine di tutti test */
	@AfterAll
	public static void closeConnection() {
		em.close();
	}
	
	@Test
	void createSoccer() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		Squadra s = new Squadra("Milan", new GregorianCalendar(1899,4,18), c);
		cls.createSoccer("Alessio", "Romagnoli", new GregorianCalendar(1995,4,17), null, s);
		Squadra s1 = ss.findTeamByName("Milan");
		int id = s1.getIdSquadra();
		Calciatore capitano = cls.getCaptain(id);
		cls.createSoccer("Davide", "Calabria", new GregorianCalendar(1996,4,17), capitano, s);
		cls.createSoccer("Suso", "Fernandez", new GregorianCalendar(1993,2,1), capitano, s);
		cls.createSoccer("Patrick", "Cutrone", new GregorianCalendar(1996,7,22), capitano, s);
		cls.createSoccer("Luigi", "Donnarumma", new GregorianCalendar(1998,1,13), capitano, s);
		Calciatore calciatore = cls.findSoccerByName("Alessio", "Romagnoli", em);
		assertEquals(capitano.equals(calciatore),true);
		long i = cls.countSoccer();
		assertEquals(5,i);
		cls.dropSoccer();
		long i1 = cls.countSoccer();
		assertEquals(0,i1);	
	}
	
	@Test
	void getSoccer() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		Squadra s = new Squadra("Milan", new GregorianCalendar(1899,4,18), c);
		cls.createSoccer("Alessio", "Romagnoli", new GregorianCalendar(1995,4,17), null, s);
		Calciatore cl = cls.findSoccerByName("Alessio", "Romagnoli", em);
		int id = cl.getIdCalciatore();
		Calciatore cl1 = cls.getSoccer(id);
		assert(cl1.equals(cl1));
		int id1 = -13;
		Calciatore cl2 = cls.getSoccer(id1);
		assertEquals(cl2,null);
		Calciatore cl3 = cls.findSoccerByName(null, "Romagnoli", em);
		assertEquals(cl3,null);	
	}
	
	@Test
	void getAllSoccers() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		Squadra s = new Squadra("Milan", new GregorianCalendar(1899,4,18), c);
		cls.createSoccer("Alessio", "Romagnoli", new GregorianCalendar(1995,4,17), null, s);
		Squadra s1 = ss.findTeamByName("Milan");
		int id = s1.getIdSquadra();
		Calciatore capitano = cls.getCaptain(id);
		cls.createSoccer("Davide", "Calabria", new GregorianCalendar(1996,4,17), capitano, s);
		cls.createSoccer("Suso", "Fernandez", new GregorianCalendar(1993,2,1), capitano, s);
		cls.createSoccer("Patrick", "Cutrone", new GregorianCalendar(1996,7,22), capitano, s);
		cls.createSoccer("Luigi", "Donnarumma", new GregorianCalendar(1998,1,13), capitano, s);
		List<Calciatore> calciatori = cls.getAllSoccer();
		int i = calciatori.size();
		assertEquals(5,i);	
	}
	
	@Test
	void getAllSoccersByTeam() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		Squadra s = new Squadra("Milan", new GregorianCalendar(1899,4,18), c);
		cls.createSoccer("Alessio", "Romagnoli", new GregorianCalendar(1995,4,17), null, s);
		Squadra s1 = ss.findTeamByName("Milan");
		int id = s1.getIdSquadra();
		Calciatore capitano = cls.getCaptain(id);
		cls.createSoccer("Davide", "Calabria", new GregorianCalendar(1996,4,17), capitano, s);
		cls.createSoccer("Suso", "Fernandez", new GregorianCalendar(1993,2,1), capitano, s);
		cls.createSoccer("Patrick", "Cutrone", new GregorianCalendar(1996,7,22), capitano, s);
		cls.createSoccer("Luigi", "Donnarumma", new GregorianCalendar(1998,1,13), capitano, s);
		
		Squadra s2 = new Squadra("Inter", new GregorianCalendar(1948,5,10), c);
		cls.createSoccer("Mauro", "Icardi", new GregorianCalendar(1995,4,17), null, s2);
		Squadra s3 = ss.findTeamByName("Inter");
		int id1 = s3.getIdSquadra();
		Calciatore capitano1 = cls.getCaptain(id1);
		cls.createSoccer("Radja", "Nainggolan", new GregorianCalendar(1996,4,17), capitano1, s2);
		cls.createSoccer("Matteo", "Politano", new GregorianCalendar(1993,2,1), capitano1, s2);
		cls.createSoccer("Milan", "Skriniar", new GregorianCalendar(1998,1,13), capitano1, s2);
		List<Calciatore> calciatori = cls.findTeamSoccerPlayer(id);
		int i = calciatori.size();
		assertEquals(5,i);
		List<Calciatore> calciatori1 = cls.findTeamSoccerPlayer(id1);
		int i1 = calciatori1.size();
		assertEquals(4,i1);	
		int id2 = -13;
		List<Calciatore> calciatori2 = cls.findTeamSoccerPlayer(id2);
		assertEquals(calciatori2,null);
	}
	
	@Test
	void updateSoccerTeam() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		Squadra s = new Squadra("Milan", new GregorianCalendar(1899,4,18), c);
		cls.createSoccer("Alessio", "Romagnoli", new GregorianCalendar(1995,4,17), null, s);
		ss.createTeam("Inter", new GregorianCalendar(1899,4,18), c);
		Calciatore cl = cls.findSoccerByName("Alessio", "Romagnoli", em);
		int idCalciatore = cl.getIdCalciatore();
		Squadra s1 = ss.findTeamByName("Inter");
		int idSquadra = s1.getIdSquadra();
		cls.updateSoccerTeam(idCalciatore, idSquadra);
		assertEquals(cl.getSquadra().getIdSquadra(),idSquadra);
		int idCalciatore1 = -13;
		boolean b = cls.updateSoccerTeam(idCalciatore1, idSquadra);
		assertEquals(b,false);	
		int idSquadra1 = -13;
		boolean b1 = cls.updateSoccerTeam(idCalciatore, idSquadra1);
		assertEquals(b1,false);
	}
	
	@Test
	void updateTeamCaptain() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		Squadra s = new Squadra("Milan", new GregorianCalendar(1899,4,18), c);
		cls.createSoccer("Alessio", "Romagnoli", new GregorianCalendar(1995,4,17), null, s);
		Squadra s1 = ss.findTeamByName("Milan");
		int idSquadra = s1.getIdSquadra();
		Calciatore capitano = cls.getCaptain(idSquadra);
		cls.createSoccer("Davide", "Calabria", new GregorianCalendar(1996,4,17), capitano, s);
		cls.createSoccer("Suso", "Fernandez", new GregorianCalendar(1993,2,1), capitano, s);
		cls.createSoccer("Patrick", "Cutrone", new GregorianCalendar(1996,7,22), capitano, s);
		cls.createSoccer("Luigi", "Donnarumma", new GregorianCalendar(1998,1,13), capitano, s);
		Calciatore c1 = cls.findSoccerByName("Davide", "Calabria", em);
		int idCapitano = c1.getIdCalciatore();
		cls.updateTeamCaptain(idCapitano, idSquadra, em);
		List<Calciatore> calciatori = cls.findTeamSoccerPlayer(idSquadra);
		for(Calciatore socc : calciatori){
			assertEquals(socc.getCapitano().getIdCalciatore(),idCapitano);
		}
		int idCapitano1 = -13;
		boolean b = cls.updateTeamCaptain(idCapitano1, idSquadra, em);
		assertEquals(b,false);	
		int idSquadra1 = -13;
		boolean b1 = cls.updateTeamCaptain(idCapitano, idSquadra1, em);
		assertEquals(b1,false);
		Squadra s2 = new Squadra("Inter", new GregorianCalendar(1948,5,10), c);
		cls.createSoccer("Mauro", "Icardi", new GregorianCalendar(1995,4,17), null, s2);
		Calciatore c2 = cls.findSoccerByName("Mauro", "Icardi", em);
		int idCapitano2 = c2.getIdCalciatore();
		boolean b2 =cls.updateTeamCaptain(idCapitano2, idSquadra, em);
		assertEquals(b2,false);
		Calciatore capitano2 = cls.getCaptain(idSquadra1);
		assertEquals(capitano2,null);
	}
	
	@Test
	void deleteSoccer() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		Squadra s = new Squadra("Milan", new GregorianCalendar(1899,4,18), c);
		cls.createSoccer("Alessio", "Romagnoli", new GregorianCalendar(1995,4,17), null, s);
		Calciatore socc = cls.findSoccerByName("Alessio", "Romagnoli", em);
		cls.deleteSoccer(socc.getIdCalciatore());
		Throwable exception = assertThrows(javax.persistence.NoResultException.class, () 
				-> {  cls.getSoccer(socc.getIdCalciatore()); });
		int id = -13;
		boolean b = cls.deleteSoccer(id);
		assertEquals(b,false);	
	}
	
	@Test
	void setter() {
		Nazione n = new Nazione("Italia","Europa");
		Campionato c = new Campionato("SerieA", n);
		Squadra s = new Squadra("Milan", new GregorianCalendar(1899,4,18), c);
		Calciatore cl = new Calciatore();
		cl.setNome("Alessio");
		cl.setCognome("Romagnoli");
		cl.setDataNascita(new GregorianCalendar(1995,4,17));
		cl.setSquadra(s);
	}
}
