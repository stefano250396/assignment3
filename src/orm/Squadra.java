package orm;
import javax.persistence.*;
import java.util.GregorianCalendar;

@Entity
@Table(name="SQUADRA")
public class Squadra {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="IDSQUADRA")
	private int idSquadra;
	@Column(name="NOMESQUADRA",unique=true)
	private String NomeSquadra;

	/** Costruttore vuoto di default */
	public Squadra(){
	}
	
	/** Costruttore parametrizzato */
	public Squadra(String NomeSquadra, GregorianCalendar DataFondazione, Campionato Campionato){
		this.NomeSquadra = NomeSquadra;
		this.DataFondazione = DataFondazione;
		this.Campionato = Campionato;
	}
	
	@Column(name="DATAFONDAZIONE") 
	@Temporal(TemporalType.DATE) 
	private GregorianCalendar DataFondazione;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="CAMPIONATO",referencedColumnName="IDCAMPIONATO") 
	private Campionato Campionato;

	
	/** Getters e setters */
	public String getNomeSquadra() {
		return NomeSquadra;
	}

	public void setNomeSquadra(String NomeSquadra) {
		this.NomeSquadra = NomeSquadra;
	}

	public GregorianCalendar getDataFondazione() {
		return DataFondazione;
	}

	public void setDataFondazione(GregorianCalendar dataFondazione) {
		DataFondazione = dataFondazione;
	}
	
	public Campionato getCampionato() {
		return Campionato;
	}

	public void setCampionato(Campionato Campionato) {
		this.Campionato = Campionato;
	}
	
	public int getIdSquadra() {
		return idSquadra;
	}

	/** Metodo per confrontare due oggetti Squadra. 
	 * Ritorna true se i due oggetti sono uguali, false altrimenti. */
	public boolean equals(Squadra s) {
		if(this.NomeSquadra==s.getNomeSquadra() && 
				this.getCampionato().getNomeCampionato().equals(s.getCampionato().getNomeCampionato())) {
			return true;
		}
		return false;
	}
}