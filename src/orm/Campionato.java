package orm;
import javax.persistence.*;

@Entity
@Table(name="CAMPIONATO")
public class Campionato {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="IDCAMPIONATO")
	private int idCampionato;
	@Column(name="NOMECAMPIONATO",unique=true)
	private String NomeCampionato;

	/** Costruttore vuoto di default */
	public Campionato(){
	}
	
	/** Costruttore parametrizzato */
	public Campionato(String NomeCampionato, Nazione Nazione){
		this.NomeCampionato = NomeCampionato;
		this.Nazione = Nazione;
	}
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="NAZIONE",referencedColumnName="IDNAZIONE") 
	private Nazione Nazione;

	/** Getters e setters */
	public String getNomeCampionato() {
		return NomeCampionato;
	}

	public void setNomeCampionato(String NomeCampionato) {
		this.NomeCampionato = NomeCampionato;
	}
	
	public Nazione getNazione() {
		return Nazione;
	}

	public void setNazione(Nazione Nazione) {
		this.Nazione = Nazione;
	}
	
	public int getIdCampionato() {
		return idCampionato;
	}

	/** Metodo per confrontare due oggetti Campionato.
	 *  Ritorna true se i due oggetti sono uguali, false altrimenti. */
	public boolean equals(Campionato c) {
		if(this.NomeCampionato==c.getNomeCampionato() && 
				this.getNazione().getNomeNazione()==c.getNazione().getNomeNazione()) {
			return true;
		}
		return false;
	}
}