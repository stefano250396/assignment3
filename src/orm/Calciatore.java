package orm;
import javax.persistence.*;

import org.eclipse.persistence.annotations.CascadeOnDelete;

import java.util.GregorianCalendar;

@Entity
@Table(name="CALCIATORE")
public class Calciatore {
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="IDCALCIATORE",nullable=true)
	private int idCalciatore;
	
	/** Costruttore vuoto di default */
	public Calciatore(){		
	}
	
	/** Costruttore parametrizzato */
	public Calciatore(String Nome, String Cognome, GregorianCalendar DataNascita, Calciatore Capitano, Squadra Squadra){
		this.Nome = Nome;
		this.Cognome = Cognome;
		this.DataNascita = DataNascita;
		this.Capitano = Capitano;
		this.Squadra = Squadra;
	}
	
	@Column(name="NOME")
	private String Nome;
	@Column(name="COGNOME")
	private String Cognome;
	@Column(name="DATANASCITA") 
	@Temporal(TemporalType.DATE) 
	private GregorianCalendar DataNascita;
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="SQUADRA",referencedColumnName="IDSQUADRA") 
	private Squadra Squadra;
	@ManyToOne
	@JoinColumn(name="CAPITANO",referencedColumnName="IDCALCIATORE")
	private Calciatore Capitano;
	
	/** Getters e Setters */
	public int getIdCalciatore() {
		return idCalciatore;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public String getCognome() {
		return Cognome;
	}

	public void setCognome(String cognome) {
		Cognome = cognome;
	}

	public GregorianCalendar getDataNascita() {
		return DataNascita;
	}

	public void setDataNascita(GregorianCalendar dataNascita) {
		DataNascita = dataNascita;
	}

	public Squadra getSquadra() {
		return Squadra;
	}

	public void setSquadra(Squadra squadra) {
		Squadra = squadra;
	}

	public Calciatore getCapitano() {
		return Capitano;
	}

	public void setCapitano(Calciatore capitano) {
		Capitano = capitano;
	}
	
	/** Metodo per confrontare due oggetti Calciatore. 
	 * Ritorna true se i due oggetti sono uguali, false altrimenti. */
	public boolean equals(Calciatore c) {
		if(this.Nome==c.getNome() && this.Cognome==c.getCognome() && 
				this.Squadra.getNomeSquadra() == c.getSquadra().getNomeSquadra() &&
				this.getCapitano().getNome() == c.getCapitano().getNome() &&
				this.getCapitano().getCognome() == c.getCapitano().getCognome()) {
			return true;
		}
		return false;
	}
	
}
