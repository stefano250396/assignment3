package orm;
import javax.persistence.*;

@Entity
@Table(name="NAZIONE")
public class Nazione {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="IDNAZIONE")
	private int idNazione;

	@Column(name="NOMENAZIONE",unique=true)
	private String NomeNazione;
	
	/** Costruttore vuoto di default */
	public Nazione(){
	}
	
	/** Costruttore parametrizzato */
	public Nazione(String NomeNazione, String Continente){
		this.NomeNazione = NomeNazione;
		this.Continente = Continente;
	}
	
	@Column(name="CONTINENTE")
	private String Continente;

	/** Getters e setters */
	public String getNomeNazione() {
		return NomeNazione;
	}

	public void setNomeNazione(String NomeNazione) {
		this.NomeNazione = NomeNazione;
	}

	public String getContinente() {
		return Continente;
	}

	public void setContinente(String continente) {
		Continente = continente;
	}
	
	public int getIdNazione() {
		return idNazione;
	}
	
	/** Metodo per confrontare due oggetti Nazione. 
	 * Ritorna true se i due oggetti sono uguali, false altrimenti. */
	public boolean equals(Nazione n) {
		if(this.NomeNazione==n.getNomeNazione() && this.Continente==n.getContinente()) {
			return true;
		}
		return false;
	}
}