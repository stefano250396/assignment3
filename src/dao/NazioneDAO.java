package dao;
import java.util.List;

import javax.persistence.EntityManager;

import orm.Nazione;

public interface NazioneDAO extends DAO<Integer, Nazione> {
	public List<Nazione> findAllNations(EntityManager em);
	
	/** Recupera un oggetto Nazione esistente a partire dall'id. */
	public Nazione getNation(int idNazione, EntityManager em);
	
	/** Recupera un oggetto Nazione esistente a partire dal nome. */
	public Nazione findNationByName(String nome, EntityManager em);
	
	/** Crea un oggetto Nazione e restituisce l'id. */
	public boolean createNation(Nazione nazione, EntityManager em);
	
	/** Aggiorna un oggetto Squadra esistente. */
	public boolean updateNationName(Nazione nazione, String nome, EntityManager em);
	
	/** Cancella un oggetto Squadra esistente. */
	public boolean deleteNation(Nazione nazione, EntityManager em);
	
	/** Conta tutti i record presenti all'interno della tabella **/
	public long countNation(EntityManager em);
	
	/** Elimina tutti i  presenti all'interno della tabella **/
	public void dropNation(EntityManager em);

}
