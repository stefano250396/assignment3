package dao;
import java.util.*;

import javax.persistence.EntityManager;

import orm.Campionato;

public interface CampionatoDAO extends DAO<Integer, Campionato>  {
	/** Recupera tutti gli oggetti Campionato dal DB. */
	public List<Campionato> findAllChampionship(EntityManager em);
	
	/** Recupera un oggetto Campionato esistente a partire dall'id. */
	public Campionato getChampionship(int idCampionato, EntityManager em);
	
	/** Recupera un oggetto Campionato esistente a partire dal nome. */
	public Campionato findChampionshipByName(String NomeCampionato, EntityManager em);
	
	/** Crea un oggetto Campionato e restituisce l'id. */
	public boolean createChampionship(Campionato campionato, EntityManager em);
	
	/** Aggiorna un oggetto Campionato esistente. */
	public boolean updateChampionshipName(Campionato campionato, String nome, EntityManager em);
	
	/** Cancella un oggetto Campionato esistente. */
	public boolean deleteChampionship(Campionato campionato, EntityManager em);

	/** Conta tutti i record presenti all'interno della tabella **/
	public long countChampionship(EntityManager em);
	
	/** Elimina tutti i  presenti all'interno della tabella **/
	public void dropChampionship(EntityManager em);
}
