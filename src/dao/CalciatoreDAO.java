package dao;
import orm.*;
import java.util.*;
import javax.persistence.EntityManager;

public interface CalciatoreDAO extends DAO<Integer, Calciatore> {
	public List<Calciatore> findTeamSoccerPlayer(Squadra squadra, EntityManager em);
	
	/** Recupera tutti gli oggetti Calciatore dal DB. */
	public List<Calciatore> findAllSoccer(EntityManager em);
	
	/** Recupera un oggetto Calciatore esistente a partire dall'id. */
	public Calciatore getSoccer(int idCalciatore, EntityManager em);
	
	/** Recupera il capitano a partire dall'id della Squadra. */
	public Calciatore getCaptain(int idSquadra, EntityManager em);
	
	/** Recupera un oggeto Calciatore esistente a partire dal suo Nome e Cognome. */
	public Calciatore findSoccerByName(String Nome, String Cognome, EntityManager em);
	
	/** Crea un oggetto Calciatore e restituisce l'id. */
	public boolean createSoccer(Calciatore calciatore, EntityManager em);
	
	/** Aggiorna la Squadra di un oggetto Calciatore esistente. */
	public boolean updateSoccerTeam(Calciatore calciatore, Squadra squadra, EntityManager em);
	
	/** Aggiorna il Capitano dei Calciatori di una Squadra esistente. */
	public boolean updateTeamCaptain(Calciatore capitano, Squadra squadra, EntityManager em);
	
	/** Cancella un oggetto Calciatore esistente. */
	public boolean deleteSoccer(Calciatore calciatore, EntityManager em);
	
	/** Conta tutti i record presenti all'interno della tabella **/
	public long countSoccer(EntityManager em);
	
	/** Elimina tutti i  presenti all'interno della tabella **/
	public void dropSoccer(EntityManager em);
}
