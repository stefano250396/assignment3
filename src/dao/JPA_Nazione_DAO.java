package dao;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import orm.Calciatore;
import orm.Nazione;

public class JPA_Nazione_DAO extends JPA_DAO<Integer, Nazione> implements NazioneDAO {

	@Override
	public List<Nazione> findAllNations(EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT n FROM " + entityClass.toString().substring(10) + " n");
		}catch(Exception e) {}
		return (List<Nazione>) q.getResultList();
	}

	@Override
	public Nazione getNation(int idNazione, EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT n FROM " + entityClass.toString().substring(10) + " n WHERE n.idNazione=:id").setParameter("id", idNazione);
		}catch(Exception e) {}
		return (Nazione) q.getSingleResult();
	}
	
	@Override
	public Nazione findNationByName(String Nome, EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT n FROM " + entityClass.toString().substring(10) + " n WHERE n.NomeNazione=:nome").setParameter("nome", Nome);
		}catch(Exception e) {}
		return (Nazione) q.getSingleResult();
	}
	

	@Override
	public boolean createNation(Nazione nazione, EntityManager em) {
		em.getTransaction().begin();
		em.persist(nazione);
		em.getTransaction().commit();
		return true;
	}

	@Override
	public boolean updateNationName(Nazione nazione, String nome, EntityManager em) {
		em.getTransaction().begin();
		nazione.setNomeNazione(nome);
		em.getTransaction().commit();
		return true;
	}

	@Override
	public boolean deleteNation(Nazione nazione, EntityManager em) {
		em.getTransaction().begin();
		em.remove(nazione);
		em.getTransaction().commit();
		return true;
	}

	public long countNation(EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT COUNT(n) FROM " + entityClass.toString().substring(10) + " n");
		}catch(Exception e) {}
		return (long) q.getSingleResult();
	}
	
	public void dropNation(EntityManager em) {
		Query q=null ;
		em.getTransaction().begin();
		try {
			int result = em.createQuery("DELETE FROM "+ entityClass.toString().substring(10) +" n").executeUpdate();
		}catch(Exception e) {}	
		em.getTransaction().commit();
	}
}
