package dao;
import java.lang.reflect.*;

import javax.persistence.*;

public abstract class JPA_DAO<K, E> implements DAO<K, E> {
	protected Class<E> entityClass;
 
	@PersistenceContext
	protected EntityManager entityManager;
 
	public JPA_DAO() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[1];
	}
 
	public void persist(E entity) { entityManager.persist(entity); }
 
	public void remove(E entity) { entityManager.remove(entity); }
 
	public E findById(K id) { return entityManager.find(entityClass, id); }
}
