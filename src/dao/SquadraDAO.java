package dao;
import java.util.*;

import javax.persistence.EntityManager;

import orm.Squadra;

public interface SquadraDAO extends DAO<Integer, Squadra> {
	public List<Squadra> findAllTeam(EntityManager em);
	
	/** Recupera un oggetto Squadra esistente a partire dall'id. */
	public Squadra getTeam(int idSquadra, EntityManager em);
	
	/** Recupera un Squadra Campionato esistente a partire dal nome. */
	public Squadra findTeamByName(String nome, EntityManager em);
	
	/** Crea un oggetto Squadra e restituisce l'id. */
	public boolean createTeam(Squadra squadra, EntityManager em);
	
	/** Aggiorna un oggetto Squadra esistente. */
	public boolean updateTeamName(Squadra squadra, String nome, EntityManager em);
	
	/** Cancella un oggetto Squadra esistente. */
	public boolean deleteTeam(Squadra squadra, EntityManager em);
	
	/** Conta tutti i record presenti all'interno della tabella **/
	public long countTeam(EntityManager em);
	
	/** Elimina tutti i  presenti all'interno della tabella **/
	public void dropTeam(EntityManager em);
}