package dao;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import orm.Squadra;

public class JPA_Squadra_DAO extends JPA_DAO<Integer, Squadra> implements SquadraDAO {

	@Override
	public List<Squadra> findAllTeam(EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT s FROM " + entityClass.toString().substring(10) + " s");
		}catch(Exception e) {}
		return (List<Squadra>) q.getResultList();
	}

	@Override
	public Squadra getTeam(int idSquadra, EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT s FROM " + entityClass.toString().substring(10) + " s WHERE s.idSquadra=:id").setParameter("id", idSquadra);
		}catch(Exception e) {}
		return (Squadra) q.getSingleResult();
	}
	
	@Override
	public Squadra findTeamByName(String Nome, EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT s FROM " + entityClass.toString().substring(10) + " s WHERE s.NomeSquadra=:nome").setParameter("nome", Nome);
		}catch(Exception e) {}
		return (Squadra) q.getSingleResult();
	}

	@Override
	public boolean createTeam(Squadra squadra, EntityManager em) {
		em.getTransaction().begin();
		em.persist(squadra);
		em.getTransaction().commit();
		return true;
	}

	@Override
	public boolean updateTeamName(Squadra squadra, String nome, EntityManager em) {
		em.getTransaction().begin();
		squadra.setNomeSquadra(nome);
		em.getTransaction().commit();
		return true;
	}

	@Override
	public boolean deleteTeam(Squadra squadra, EntityManager em) {
		em.getTransaction().begin();
		em.remove(squadra);
		em.getTransaction().commit();
		return true;
	}
	
	public long countTeam(EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT COUNT(s) FROM " + entityClass.toString().substring(10) + " s");
		}catch(Exception e) {}
		return (long) q.getSingleResult();
	}
	
	public void dropTeam(EntityManager em) {
		Query q=null ;
		em.getTransaction().begin();
		try {
			int result = em.createQuery("DELETE FROM "+ entityClass.toString().substring(10) +" s").executeUpdate();
		}catch(Exception e) {}	
		em.getTransaction().commit();
	}
}
