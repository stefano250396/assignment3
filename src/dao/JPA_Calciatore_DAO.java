package dao;
import java.util.List;
import javax.persistence.*;

import org.eclipse.persistence.exceptions.QueryException;

import orm.Calciatore;
import orm.Squadra;


public class JPA_Calciatore_DAO extends JPA_DAO<Integer, Calciatore> implements CalciatoreDAO {
	@Override
	public List<Calciatore> findTeamSoccerPlayer(Squadra squadra, EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT c FROM " + entityClass.toString().substring(10) + " c WHERE c.Squadra=:id").setParameter("id", squadra);
		}catch(Exception e) {}
		return (List<Calciatore>) q.getResultList();
	}
	
	@Override
	public List<Calciatore> findAllSoccer(EntityManager em){
		Query q=null;
		try {
			q = em.createQuery("SELECT c FROM " + entityClass.toString().substring(10) + " c");
		}catch(Exception e) {}
		return (List<Calciatore>) q.getResultList();
	}
	
	@Override
	public Calciatore getSoccer(int idCalciatore,EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT c FROM " + entityClass.toString().substring(10) + " c WHERE c.idCalciatore=:id").setParameter("id", idCalciatore);
		}catch(Exception e) {}
		return (Calciatore) q.getSingleResult();
	}
	
	@Override
	public Calciatore getCaptain(int idSquadra, EntityManager em) {
		Calciatore captain=null;
		Query q = em.createQuery("SELECT DISTINCT c.Capitano FROM " + entityClass.toString().substring(10) + " c");
		List<Calciatore> capitani = q.getResultList();
		for(Calciatore c : capitani) {
			if(c.getSquadra().getIdSquadra()==idSquadra)
				captain=c;
		}
		return captain;
	}
	
	@Override
	public Calciatore findSoccerByName(String Nome, String Cognome, EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT c FROM " + entityClass.toString().substring(10) + " c WHERE c.Cognome=:cognome"
					+ " AND c.Nome=:nome").setParameter("cognome", Cognome).setParameter("nome", Nome);
		}catch(Exception e) {}
		return (Calciatore) q.getSingleResult();	}
	
	@Override
	public boolean createSoccer(Calciatore calciatore,EntityManager em) {
		em.getTransaction().begin();
		em.persist(calciatore);
		em.getTransaction().commit();
		return true;
	}
	
	@Override
	public boolean updateSoccerTeam(Calciatore calciatore, Squadra squadra, EntityManager em) {	
		em.getTransaction().begin();
		Calciatore capitano=this.getCaptain(squadra.getIdSquadra(), em);
		calciatore.setSquadra(squadra);
		calciatore.setCapitano(capitano);
		em.getTransaction().commit();
		return true;
	}
	
	@Override
	public boolean updateTeamCaptain(Calciatore capitano, Squadra squadra, EntityManager em) {	
		em.getTransaction().begin();
		List<Calciatore> Calciatori = this.findTeamSoccerPlayer(squadra, em);
		for (Calciatore c : Calciatori) {
			c.setCapitano(capitano);
		}
		em.getTransaction().commit();
		return true;
	}
	
	@Override
	public boolean deleteSoccer(Calciatore calciatore,EntityManager em) {
		em.getTransaction().begin();
		em.remove(calciatore);
		em.getTransaction().commit();
		return true;
	}
	
	@Override
	public long countSoccer(EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT COUNT(s) FROM " + entityClass.toString().substring(10) + " s");
		}catch(Exception e) {}
		return (long) q.getSingleResult();
	}
	
	@Override
	public void dropSoccer(EntityManager em) {
		em.getTransaction().begin();
		List<Calciatore> calciatori = this.findAllSoccer(em);
		for(Calciatore socc : calciatori) {
			socc.setCapitano(null);
			socc.setSquadra(null);
		}
		em.getTransaction().commit();
		em.getTransaction().begin();		
	    try {
			int result = em.createQuery("DELETE FROM "+ entityClass.toString().substring(10) +" s").executeUpdate();
		}catch(Exception e) {}	
	    em.getTransaction().commit();
	}
}