package dao;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import orm.Campionato;

public class JPA_Campionato_DAO extends JPA_DAO<Integer, Campionato> implements CampionatoDAO {

	@Override
	public List<Campionato> findAllChampionship(EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT c FROM " + entityClass.toString().substring(10) + " c");
		}catch(Exception e) {}
		return (List<Campionato>) q.getResultList();
	}

	@Override
	public Campionato getChampionship(int idCampionato, EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT c FROM " + entityClass.toString().substring(10) + " c WHERE c.idCampionato=:id").setParameter("id", idCampionato);;
		}catch(Exception e) {}
		return (Campionato) q.getSingleResult();
	}
	
	@Override
	public Campionato findChampionshipByName(String Nome, EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT c FROM " + entityClass.toString().substring(10) + " c WHERE c.NomeCampionato=:nome").setParameter("nome", Nome);
		}catch(Exception e) {}
		return (Campionato) q.getSingleResult();
	}

	@Override
	public boolean createChampionship(Campionato campionato, EntityManager em) {
		em.getTransaction().begin();
		em.persist(campionato);
		em.getTransaction().commit();
		return true;
	}

	@Override
	public boolean updateChampionshipName(Campionato campionato, String nome, EntityManager em) {
		em.getTransaction().begin();
		campionato.setNomeCampionato(nome);
		em.getTransaction().commit();
		return true;
	}

	@Override
	public boolean deleteChampionship(Campionato campionato, EntityManager em) {
		em.getTransaction().begin();
		em.remove(campionato);
		em.getTransaction().commit();
		return true;
	}
	
	public long countChampionship(EntityManager em) {
		Query q=null;
		try {
			q = em.createQuery("SELECT COUNT(c) FROM " + entityClass.toString().substring(10) + " c");
		}catch(Exception e) {}
		return (long) q.getSingleResult();
	}
	
	public void dropChampionship(EntityManager em) {
		Query q=null ;
		em.getTransaction().begin();
		try {
			int result = em.createQuery("DELETE FROM "+ entityClass.toString().substring(10) +" c").executeUpdate();
		}catch(Exception e) {}	
		em.getTransaction().commit();
	}

}
