# Assignment3 - Processo e sviluppo software

La **documentazione del progetto** è stata caricata nell'apposita sezione di consegna su elearning.

### Istruzioni per eseguire test dell'assignment
Al fine di eseguire la suite di test, è necessario svolgere le seguenti operazioni:
1. Aprire **Eclipse 2018-09 (4.9.0)**
2. Creare un nuovo progetto JPA nel seguente modo: *File* -> *New* -> *JPA Project*
3. Sotto la voce JPA_Version selezionare la **versione 2.1**
4. Sotto la voce Target runtime selezionare **JRE 1.8 System Library (JavaSE-1.8)**
5. Fare click su *Next* e successivamente ancora su *Next*
6. Se non viene automaticamente inclusa scaricare la libreria **EclipseLink 2.5.2**:
    * Fare click sul floppy disk e selezionare la libreria **EclipseLink 2.5.2**
7. Fare click su *Finish*
8. Infine occorre aggiungere le librerie **MySQL Connector 8.0.13** e **JUnit 5**
    * tasto destro sulla cartella di progetto *Build Path* -> *Add Libraries* -> *JUnit* > *Junit Library Version* > *Junit5* > *Finish* 
    * per MySQL Connector 8.0.13 scaricarlo tramite questo [link](https://mvnrepository.com/artifact/mysql/mysql-connector-java/8.0.13)
    * tasto destro sulla cartella di progetto *Build Path* -> *Add External Archives* -> Selezionare il file appena scaricato

Una volta fatto ciò, selezionare tutti i file presenti nella **cartella src** della repository e copiarli all'interno della **cartella src** 
del nuovo progetto appena creato. Verrà chiesto di sovrascrivere il file **persistence.xml**, selezionare **overwrite**.
Inoltre nel file **persistence.xml** presente nella cartella **META-INF** modificare i valori delle property *javax.persistence.jdbc.user* e
*javax.persistence.jdbc.password* con i relativi **USERNAME** e **PASSWORD** del proprio SQL Server.

L'ultimo passo prima di eseguire i test richiede di aprire la console **cmd** e di accedere alla console di **MySQL** lanciando questo comando:
*  SET GLOBAL time_zone = '-6:00';

Questo va fatto poichè una volta eseguito il progetto verrebbe altrimenti scatenata un eccezione di tipo **serverTimeZone**.

É ora possibile eseguire il package di test nel seguente modo:
* Tasto destro sul package **test** -> *Run as* -> *JUnit test*



